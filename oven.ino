#include <Fsm.h>
#include <ezButton.h>
#include <TM1637Display.h>

#define OVEN_LIGHT 2
#define BEEPER 3
#define DOOR_SWITCH 4
#define GREEN_SWITCH 5
#define BLUE_SWITCH 6
#define YELLOW_SWITCH 7
#define RED_SWITCH 8
#define CLK 9
#define DIO 10
#define COLON 0b01000000


ezButton doorSwitch(DOOR_SWITCH);
ezButton redSwitch(RED_SWITCH);
ezButton greenSwitch(GREEN_SWITCH);
ezButton blueSwitch(BLUE_SWITCH);
ezButton yellowSwitch(YELLOW_SWITCH);

TM1637Display display = TM1637Display(CLK, DIO);

//fsm triggers
enum Trigger {
  RED_TOGGLE,
  GREEN_TOGGLE,
  BLUE_TOGGLE,
  YELLOW_TOGGLE,
  DOOR_OPENING,
  DOOR_CLOSING,
  RUN_DONE,
  DO_CLICK,
};

int runTime = 0;

void displayTime(){
  int d;
  d = ((runTime / 60) * 100) + (runTime % 60);
  display.showNumberDecEx(d, COLON, true);
}

//fsm state functions
void idle_entry() {
  Serial.println("idle");
  digitalWrite(OVEN_LIGHT, LOW);
}

void run_entry() {
  Serial.println("run");
  digitalWrite(OVEN_LIGHT, HIGH);
}

void done_entry(){
  Serial.println("done");
  digitalWrite(BEEPER, HIGH);
}

void done_exit(){
  digitalWrite(BEEPER, LOW);
}

void open_entry() {
  Serial.println("open");
  digitalWrite(OVEN_LIGHT, HIGH);
}

//fsm states
State state_idle(idle_entry, NULL, NULL);
State state_run(run_entry, checkTime, NULL);
State state_open(open_entry, NULL, NULL);

State state_beeperIdle(NULL, NULL, NULL);
State state_click(done_entry, NULL, done_exit);
State state_beep1(done_entry, NULL, done_exit);
State state_beep2(done_entry, NULL, done_exit);
State state_beep3(done_entry, NULL, done_exit);
State state_off1(NULL, NULL, NULL);
State state_off2(NULL, NULL, NULL);

//You can have as many states as you'd like


//define the fsm with the state it will start in
Fsm fsm(&state_idle);
Fsm beeper(&state_beeperIdle);

void checkTime(){
  if (runTime == 0){
    fsm.trigger(RUN_DONE);
    beeper.trigger(RUN_DONE);
  }
}

void timeDec1s(){
  runTime--;
  Serial.println(runTime, DEC);
  displayTime();
}

void timeInc1m(){
  runTime+=60;
  if (runTime > 300) {
    runTime = 300;
  }
  Serial.println(runTime, DEC);
  displayTime();
}

void timeInc(){
  runTime+=10;
  if (runTime > 300) {
    runTime = 300;
  }
  Serial.println(runTime, DEC);
  displayTime();
}

void timeDec(){
  if (runTime > 10){
    runTime-=10;
  } else {
    runTime = 0;
  }
  Serial.println(runTime, DEC);
  displayTime();
}

void starting(){
  if (runTime == 0) {
    runTime = 60;
  }
  Serial.println(runTime, DEC);
  displayTime();
}

void stopping(){
  runTime = 0;
  Serial.println(runTime, DEC);
  displayTime();
}

void initfsm() {
  fsm.add_transition(&state_idle, &state_open, DOOR_OPENING, NULL);
  fsm.add_transition(&state_idle, &state_idle, BLUE_TOGGLE, timeInc);
  fsm.add_transition(&state_idle, &state_idle, YELLOW_TOGGLE, timeDec);
  fsm.add_transition(&state_idle, &state_run, GREEN_TOGGLE, starting);
  fsm.add_transition(&state_idle, &state_idle, RED_TOGGLE, stopping);
  fsm.add_transition(&state_run, &state_idle, RED_TOGGLE, stopping);
  fsm.add_transition(&state_run, &state_open, DOOR_OPENING, NULL);
  fsm.add_transition(&state_run, &state_idle, RUN_DONE, NULL);
  fsm.add_transition(&state_run, &state_run, GREEN_TOGGLE, timeInc1m);
  fsm.add_transition(&state_open, &state_idle, DOOR_CLOSING, NULL);
  fsm.add_timed_transition(&state_run, &state_run, 1000, timeDec1s);  

  beeper.add_transition(&state_beeperIdle, &state_click, DO_CLICK, NULL);
  beeper.add_transition(&state_beeperIdle, &state_beep1, RUN_DONE, NULL);
  beeper.add_timed_transition(&state_click, &state_beeperIdle, 5, NULL);
  beeper.add_timed_transition(&state_beep1, &state_off1, 500, NULL);
  beeper.add_timed_transition(&state_off1, &state_beep2, 500, NULL);
  beeper.add_timed_transition(&state_beep2, &state_off2, 500, NULL);
  beeper.add_timed_transition(&state_off2, &state_beep3, 500, NULL);
  beeper.add_timed_transition(&state_beep3, &state_beeperIdle, 500, NULL);
}

void handleButton(ezButton *button, Trigger trigger){
  button->loop();
  if (button->isPressed()){
    
    fsm.trigger(trigger);
    beeper.trigger(DO_CLICK);
  }
}

void setup() {
  Serial.begin(9600);
  Serial.flush();
  
  pinMode(OVEN_LIGHT, OUTPUT);
  pinMode(BEEPER, OUTPUT);
  pinMode(LED_BUILTIN, OUTPUT);
  
  doorSwitch.setDebounceTime(100);
  redSwitch.setDebounceTime(100);
  greenSwitch.setDebounceTime(100);
  blueSwitch.setDebounceTime(100);
  yellowSwitch.setDebounceTime(100);

  display.clear();
  display.setBrightness(7);
  displayTime();
  //init fsm
  initfsm();
}

void loop() {

  fsm.run_machine();
  beeper.run_machine();

  doorSwitch.loop();
  if (doorSwitch.isPressed()){
    fsm.trigger(DOOR_CLOSING);
  }
  if (doorSwitch.isReleased()){
    fsm.trigger(DOOR_OPENING);
  }

  handleButton(&redSwitch, RED_TOGGLE);
  handleButton(&greenSwitch, GREEN_TOGGLE);
  handleButton(&yellowSwitch, YELLOW_TOGGLE);
  handleButton(&blueSwitch, BLUE_TOGGLE);
  
  if(Serial.available()){
    char c;
    
    c = char(Serial.read());
    switch (c) {
      case 'r':
        fsm.trigger(RED_TOGGLE);
        break;
      case 'g':
        fsm.trigger(GREEN_TOGGLE);
        break;
      case 'y':
        fsm.trigger(YELLOW_TOGGLE);
        break;      
      case 'b':
        fsm.trigger(BLUE_TOGGLE);
        break;      
      case 'd':
        fsm.trigger(DOOR_OPENING);
        break;      
      case 'D':
        fsm.trigger(DOOR_CLOSING);
        break;      
    }
  }
}
