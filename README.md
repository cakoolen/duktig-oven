# Oven 
Oven is a toy for upgrading the oven function of the IKEA DUKTIG (kids kitchen).

## State diagram

The software is implemented as a state machine:
```plantuml
@startuml

[*] --> Idle
Idle --> Open: doorOpening
Open --> Idle: doorClosing
Idle --> Run: greenButton {if t==0 then t = 60}
Run --> Open: doorOpening
Run --> Run: after 1s {t -= 1}
Run --> Run: greenButton {t += 60}
Run --> Idle: isDone
Run --> Idle: redButton {t = 0}
Idle --> Idle: yellowButton {t -= 10}\nblueButton {t += 10}\nredButton {t = 0}

Idle: **Entry:** ovenLight Off
Open: **Entry:** ovenLight On
Run: **Entry:** ovenLight On

[*] --> buttonIdle
buttonIdle --> click: click
click --> buttonIdle: after 10ms
click: **Entry:** beep On
click: **Exit:** beep Off

buttonIdle --> beep1: isDone
beep1 --> off1: after 500ms
off1 --> beep2: after 500ms
beep2 --> off2: after 500ms
off2 --> beep3: after 500ms
beep3 --> buttonIdle: after 500ms
@enduml
```